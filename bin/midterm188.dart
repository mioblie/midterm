import 'dart:io';
import 'dart:math';

import 'package:midterm188/midterm188.dart' as midterm188;

splitString(List number, String word) {
  return number = word.split(" ");
}

void main(List<String> arguments) {
  List number = [];
  print("Enter your word : ");
  String? w = (stdin.readLineSync()!);
  String word = w;
  List<String> x = splitString(number, word);
  print(x); //Output Ex.1
  List<String> postfix = InToPo(x);
  print(postfix); //Output Ex.2
  double sun = Evaluate_Postfix(postfix);
  print(sun);
}

InToPo(var x) {
  List<String> operator = [];
  List<String> postfix = [];
  int pot1 = 0;
  int pot2 = 0;

  for (int i = 0; i < x.length; i++) {
    if (x[i] == "0" ||
        x[i] == "1" ||
        x[i] == "2" ||
        x[i] == "3" ||
        x[i] == "4" ||
        x[i] == "5" ||
        x[i] == "6" ||
        x[i] == "7" ||
        x[i] == "8" ||
        x[i] == "9") {
      postfix.add(x[i]);
    }

    if (x[i] == "+" ||
        x[i] == "-" ||
        x[i] == "*" ||
        x[i] == "/" ||
        x[i] == "^") {
      if (x[i] == "(" || x[i] == ")") {
        pot1 = 0;
      } else if (x[i] == "+" || x[i] == "-") {
        pot1 = 1;
      } else if (x[i] == "*" || x[i] == "/") {
        pot1 = 2;
      } else if (x[i] == "^") {
        pot1 = 3;
      }
      if (operator.length > 0) {
        if (operator.last == "(" || operator.last == ")") {
          pot2 = 0;
        } else if (operator.last == "+" || operator.last == "-") {
          pot2 = 1;
        } else if (operator.last == "*" || operator.last == "/") {
          pot2 = 2;
        } else if (operator.last == "^") {
          pot2 = 3;
        }
      }
      while (operator.isNotEmpty && operator.last != "(" && pot1 <= pot2) {
        String stack = operator.last;
        operator.removeLast();
        postfix.add(stack);
      }
      operator.add(x[i]);
    }
    if (x[i] == "(") {
      operator.add(x[i]);
    }
    if (x[i] == ")") {
      while (operator.last != "(") {
        String stack = operator.last;
        operator.removeLast();
        postfix.add(stack);
      }
      operator.remove("(");
    }
  }
  while (operator.isNotEmpty) {
    String stack = operator.last;
    operator.removeLast();
    postfix.add(stack);
  }
  return postfix;
}

Evaluate_Postfix(var postfix) {
  List<double> values = [];
  double sum;
  double A;
  for (int i = 0; i < postfix.length; i++) {
    if (postfix[i] == "0" ||
        postfix[i] == "1" ||
        postfix[i] == "2" ||
        postfix[i] == "3" ||
        postfix[i] == "4" ||
        postfix[i] == "5" ||
        postfix[i] == "6" ||
        postfix[i] == "7" ||
        postfix[i] == "8" ||
        postfix[i] == "9") {
      if (postfix[i] == "0") {
        sum = 0;
        values.add(sum);
      }
      if (postfix[i] == "1") {
        sum = 1;
        values.add(sum);
      }
      if (postfix[i] == "2") {
        sum = 2;
        values.add(sum);
      }
      if (postfix[i] == "3") {
        sum = 3;
        values.add(sum);
      }
      if (postfix[i] == "4") {
        sum = 4;
        values.add(sum);
      }
      if (postfix[i] == "5") {
        sum = 5;
        values.add(sum);
      }
      if (postfix[i] == "6") {
        sum = 6;
        values.add(sum);
      }
      if (postfix[i] == "7") {
        sum = 7;
        values.add(sum);
      }
      if (postfix[i] == "8") {
        sum = 8;
        values.add(sum);
      }
      if (postfix[i] == "9") {
        sum = 9;
        values.add(sum);
      }
    } else {
      double A = 0;
      double right = values.last;
      values.removeLast();
      double left = values.last;
      values.removeLast();

      if (postfix[i] == "+") {
        A = left + right;
      }
      if (postfix[i] == "-") {
        A = left - right;
      }
      if (postfix[i] == "*") {
        A = left * right;
      }
      if (postfix[i] == "/") {
        A = left / right;
      }
      if (postfix[i] == "^") {
        A = 0.0 + pow(left, right);
      }
      values.add(A);
    }
  }
  return values[0];
}
